local sensorInfo = {
    name = "windDirection",
    desc = "Return data of actual wind.",
    author = "PepeAmpere",
    date = "2017-05-16",
    license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function()
    local dirX, dirY, dirZ, strength = SpringGetWind()

    return {
        direction = Vec3(dirX, dirY, dirZ),
        strength = strength,
    }
end